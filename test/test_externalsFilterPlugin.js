import { describe, it } from "mocha";
import { expect } from "chai";

import { externalsFilterPlugin } from "../src/index.js";


const
  external = false,
  internal = null;

describe("externalsFilterPlugin", function() {

  it("filters external modules", function() {
    const fltr = externalsFilterPlugin();
    fltr.buildStart();

    Object.entries({
      "node:module": external,
      express: external,
      "@cern/nodash": external,
      "/absolute/path": internal,
      "./relative/path": internal,
      "\0virtual_module": internal
    }).forEach(([ module, dependency ]) => {
      expect(fltr.resolveId(module), module).to.be.equal(dependency);
    });
  });

  describe("filters external modules with exceptions", function() {

    it("single exception using a string", function() {
      const fltr = externalsFilterPlugin({ include: "@cern/nodash" });
      fltr.buildStart();

      Object.entries({
        "node:module": external,
        express: external,
        "@cern/nodash": internal,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });

    it("single exception using a regular expression", function() {
      const fltr = externalsFilterPlugin({ include: /^express$/ });
      fltr.buildStart();

      Object.entries({
        "node:module": external,
        express: internal,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });

    it("multiple exceptions using a regular expression", function() {
      const fltr = externalsFilterPlugin({ include: /^node.*/ });
      fltr.buildStart();

      Object.entries({
        "node:module": internal,
        "node:fs": internal,
        "node:path": internal,
        express: external,
        "@cern/nodash": external,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });

    it("multiple exceptions using an array of strings", function() {
      const fltr = externalsFilterPlugin({
        include: [ "node:fs", "@cern/nodash" ] });
      fltr.buildStart();

      Object.entries({
        "node:module": external,
        "node:fs": internal,
        "node:path": external,
        express: external,
        "@cern/nodash": internal,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });

    it("multiple exceptions using an array of regular expressions", function() {
      const fltr = externalsFilterPlugin({
        include: [ /^node.*/, /@cern/ ]
      });
      fltr.buildStart();

      Object.entries({
        "node:module": internal,
        "node:fs": internal,
        "node:path": internal,
        express: external,
        "@cern/nodash": internal,
        "@cern/ci-utils": internal,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });

    it("multiple exceptions using an array of strings and regular expressions",
      function() {
        const fltr = externalsFilterPlugin({
          include: [ /^node.*/, "express" ]
        });
        fltr.buildStart();

        Object.entries({
          "node:module": internal,
          "node:fs": internal,
          "node:path": internal,
          express: internal,
          "@cern/nodash": external,
          "@cern/ci-utils": external,
          "/absolute/path": internal,
          "./relative/path": internal,
          "\0virtual_module": internal
        }).forEach(([ module, dependency ]) => {
          expect(fltr.resolveId(module), module).to.be.equal(dependency);
        });
      });

    it("ignores wrong inclusion", function() {
      const fltr = externalsFilterPlugin({
        // @ts-ignore: invalid inclusion on purpose
        include: [ /@cern/, null, 8, "express", undefined, { toto: NaN } ]
      });
      fltr.buildStart();

      Object.entries({
        "node:module": external,
        "node:fs": external,
        "node:path": external,
        express: internal,
        "@cern/nodash": internal,
        "@cern/ci-utils": internal,
        "/absolute/path": internal,
        "./relative/path": internal,
        "\0virtual_module": internal
      }).forEach(([ module, dependency ]) => {
        expect(fltr.resolveId(module), module).to.be.equal(dependency);
      });
    });
  });
});
