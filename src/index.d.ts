export = ViteUtils;
export as namespace ViteUtils;

declare namespace ViteUtils {

  interface Plugin {
    name: string
    version: string
    enforce?: string
    buildStart: () => void
    resolveId: (source: string) => false|null
  }
  namespace ExternalsFilterPlugin {

    interface Options {
      include?: string|RegExp|(string|RegExp)[]
    }
  }

  export function externalsFilterPlugin(
    options?: ExternalsFilterPlugin.Options): Plugin
}
