// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-03-01T10:30:17
// SPDX-FileContributor: Author: Gabriele De Blasi <gabriele.de.blasi@cern.ch>

import { isAbsolute } from "node:path";

/**
 * @typedef {ViteUtils.ExternalsFilterPlugin.Options} Options
 *
 * @typedef {Array<(filename: string) => boolean>} Include
 */


/** @type {Required<Options>} */
const defaults = {
  include: []
};

/**
 * A Vite/Rollup plug-in with the purpose of excluding by default all
 * dependencies of an application from the bundle. In other words, only source
 * files get included in the application bundle. It also allows to force the
 * inclusion of particular modules in the bundle through its options.
 */


/**
 * @param {Options} options
 * @returns {ViteUtils.Plugin}
 */
function externalsFilter(options = {}) {
  const config = { ...defaults, ...options };
  /** @type {Include} */
  let include;

  /** @param {string} src */
  function isIncluded(src) {
    return include.some((fun) => fun(src));
  }

  return {
    name: "externals-filter",
    version: "1.0.0",
    enforce: "pre", // have to be applied before 'vite:resolve' (core plugin)
    buildStart() {
      // transform the 'include' option into an array of boolean test functions
      include = /** @type {(string|RegExp)[]} */([]).concat(config.include)
      .reduce((ret, inc) => {
        if (inc instanceof RegExp) {
          ret.push((str) => inc.test(str));
        }
        else if (typeof inc === "string" && inc.length > 0) {
          ret.push((str) => str === inc);
        }
        else {
          // @ts-ignore: https://rollupjs.org/plugin-development/#this-warn
          this.warn = this.warn ?? console.warn;
          this.warn("[Externals Filter Plugin] Invalid inclusion: " +
            JSON.stringify(inc));
        }
        return ret;
      }, /** @type {Include} */([]));
    },
    /**
     * @param {string} source - source module to import from
     * @returns {false|null}
     */
    resolveId(source) {
      // Let's defer resolution of sources:
      // - having absolute and relative paths
      // - prefixed with '\0' (https://vitejs.dev/guide/api-plugin#virtual-modules-convention)
      // - required to be included through the 'include' option
      if (/^(?:\0|\.{0,2}\/)/.test(source) || isAbsolute(source) ||
        isIncluded(source)) {
        return null;
      }

      return false; // external module
    }
  };
}

export default externalsFilter;
export { externalsFilter };
