# Vite Utils

A set of Vite utilities for the development of Node.js applications.


## Installation

To install these utilities in your project do:

```bash
echo '@cern:registry=https://gitlab.cern.ch/api/v4/groups/mro/-/packages/npm/' > .npmrc

npm install --save-dev @cern/vite-utils
```

## Plugins

### ExternalFilter

A plug-in with the purpose of excluding by default all dependencies of
an application from the bundle. In other words, only the source files of an
application get included in the application bundle.

It also allows to force the inclusion of particular modules in the bundle
through its options.

#### Usage

In the project's `vite.config.js` file, you can simply import the plugin and
use it, as the following example:

```js
import { externalsFilterPlugin } from "@cern/vite-utils";

//...

export default defineConfig(({ mode }) => {
  return {
    plugins: [ externalsFilterPlugin() ],
    // vite config...
  };
});
```

It is possible to provide some options to change the default plugin behavior:

```js
import { externalsFilterPlugin } from "@cern/vite-utils";

//...

export default defineConfig(({ mode }) => {
  return {
    plugins: [ externalsFilterPlugin({
      include: [ "@cern/nodash" ]
    }) ],
    // vite config...
  };
});
